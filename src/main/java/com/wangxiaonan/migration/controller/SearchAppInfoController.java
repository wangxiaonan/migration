package com.wangxiaonan.migration.controller;

import com.alibaba.fastjson.JSONObject;
import com.wangxiaonan.migration.domain.CollectAppInfoBus;
import com.wangxiaonan.migration.domain.SearchParams;
import com.wangxiaonan.migration.dto.GlobalProgressList;
import com.wangxiaonan.migration.dto.Progress;
import com.wangxiaonan.migration.service.SearchAppInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Map;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
@Controller
@RequestMapping("/app-info")
public class SearchAppInfoController {
    private static final Logger logger = LoggerFactory.getLogger(SearchAppInfoController.class);

    @Autowired
    private SearchAppInfoService searchAppInfoService;

    @RequestMapping("/index")
    public String index() {
        return "/index";
    }

    @RequestMapping("/_search")
    public String searchAppInfo(HttpServletRequest request, HttpServletResponse response, Model model,
                                @PageableDefault(page = 0, size = 10) Pageable pageable) {
//        SearchParams searchParams = new SearchParams();
//        searchParams.setContents("body@*collectAppInfo*");
//        searchParams.setIndices("logstash-front-info_2018.07.16");
//        searchParams.setPageable(pageable);
//        Page<CollectAppInfo> collectAppInfos = searchAppInfoService.searchAppInfo(CollectAppInfo.class, searchParams);
//        Page<CollectAppInfoBus> collectAppInfos = searchAppInfoService.searchAppInfo(CollectAppInfoBus.class, searchParams);
//        if (collectAppInfos == null) {
//            collectAppInfos = new PageImpl<>(new ArrayList<>(), PageRequest.of(1, 1), 1);
//        }
//        for (CollectAppInfoBus appInfo : collectAppInfos.getContent()) {
//            logger.info(appInfo.getReqParam()[0]);
//        }

        Page<CollectAppInfoBus> collectAppInfos = new PageImpl<>(new ArrayList<CollectAppInfoBus>(), PageRequest.of(0, 10), 0);
        model.addAttribute("appInfoPage", collectAppInfos);
        return "/search";
    }

    @RequestMapping("/items")
    public String jsonAppInfo(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "indices") String indices,
                              @RequestParam(value = "contents") String contents, @PageableDefault(page = 0, size = 10) Pageable pageable) {
        SearchParams searchParams = new SearchParams();
        searchParams.setContents(contents);
        searchParams.setIndices(indices);
        searchParams.setPageable(pageable);
        Page<CollectAppInfoBus> collectAppInfos = searchAppInfoService.searchAppInfo(CollectAppInfoBus.class, searchParams);
        if (collectAppInfos == null) {
            collectAppInfos = new PageImpl<>(new ArrayList<>(), PageRequest.of(1, 1), 1);
        }
        for (CollectAppInfoBus appInfo : collectAppInfos.getContent()) {
            logger.info(appInfo.getReqParam()[0]);
        }
        model.addAttribute("appInfoPage", collectAppInfos);
        return "/items";
    }

    @RequestMapping("/migration")
    public String migrationData(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "indices") String indices,
                                @RequestParam(value = "contents") String contents, @PageableDefault(page = 0, size = 100) Pageable pageable) {
        SearchParams searchParams = new SearchParams();
        searchParams.setContents(contents);
        searchParams.setIndices(indices);
        searchParams.setPageable(pageable);
        searchParams.setScroll("5m");

        try {
            String result = searchAppInfoService.migrationAppInfoByScroll(CollectAppInfoBus.class, searchParams);
            model.addAttribute("message", result);
            model.addAttribute("status", 200);
        } catch (Exception e) {
            model.addAttribute("status", 500);
            model.addAttribute("message", e.getMessage());
            logger.error("[migrationData] migrationAppInfoByScroll ==> error ", e);
        }

        return "/result";
    }

    @RequestMapping("/migration/progress")
    @ResponseBody
    public String migrationProgress(HttpServletRequest request, HttpServletResponse response) {
        JSONObject rs = new JSONObject();
        Map<String, Progress> progressMap = GlobalProgressList.getInstance();

        Progress progress = progressMap.get(GlobalProgressList.PROGRESS_NAME_APP_INFO);

        JSONObject p = new JSONObject();
        if (progress == null) {
            progress = new Progress("", 0, 0);
        }

        p.put("total", progress.getTotal());
        p.put("curNum", progress.getCurNum());
        p.put("percent", progress.getPercent(0));

        rs.put("data", p);
        return rs.toJSONString();
    }
}
