package com.wangxiaonan.migration.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
public class CommonUtils {
    public static Date str2Date(String strDate) {
        Date date = new Date(0);
        ;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(strDate);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return date;
    }

    public static int dateInt(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String strDate = sdf.format(date);
        return Integer.valueOf(strDate);
    }

    public static String timeString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String strTime = sdf.format(date);
        return strTime;
    }

}
