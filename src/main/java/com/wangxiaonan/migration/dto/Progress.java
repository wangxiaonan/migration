package com.wangxiaonan.migration.dto;

import java.io.Serializable;
import java.text.NumberFormat;

/**
 * 某一个进度情况
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/20
 **/
public class Progress implements Serializable {
    private String name;
    private int total;
    private int curNum;

    public Progress() {
    }

    public Progress(String name, int total, int curNum) {
        this.name = name;
        this.total = total;
        this.curNum = curNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCurNum() {
        return curNum;
    }

    public void setCurNum(int curNum) {
        this.curNum = curNum;
    }

    public void init(String name) {
        this.total = 0;
        this.curNum = 0;
        this.name = name;
    }

    public String getPercent(int length) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(length);
        String percent = "0";
        if (total != 0 && curNum != 0) {
            percent = numberFormat.format((float) curNum / (float) total * 100);
        }
        return String.valueOf(percent);
    }

}
