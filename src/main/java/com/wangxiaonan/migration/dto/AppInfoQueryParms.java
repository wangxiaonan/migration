package com.wangxiaonan.migration.dto;

import java.util.List;
import java.util.Map;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
public class AppInfoQueryParms {
    private List<String> indices;
    private String type;
    private int size;
    private int page;
    private int total;
    /**
     * 注意游标查询每次返回一个新字段 _scroll_id`。
     * 每次我们做下一次游标查询， 我们必须把前一次查询返回的字段 `_scroll_id 传递进去。
     * 当没有更多的结果返回的时候，我们就处理完所有匹配的文档了。
     */
    private String scrollId;
    /**
     * 启用游标查询可以通过在查询的时候设置参数 scroll 的值为我们期望的游标查询的过期时间。
     * 游标查询的过期时间会在每次做查询的时候刷新，所以这个时间只需要足够处理当前批的结果就可以了，
     * 而不是处理查询结果的所有文档的所需时间。这个过期时间的参数很重要，因为保持这个游标查询窗口需要消耗资源，
     * 所以我们期望如果不再需要维护这种资源就该早点儿释放掉。
     * 设置这个超时能够让 Elasticsearch 在稍后空闲的时候自动释放这部分资源。
     */
    private String scroll;
    private Map<String, String> queryContent;

    public List<String> getIndices() {
        return indices;
    }

    public void setIndices(List<String> indices) {
        this.indices = indices;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    /**
     * from 0
     *
     * @param page
     */
    public void setPage(int page) {
        this.page = page;
    }

    public Map<String, String> getQueryContent() {
        return queryContent;
    }

    public void setQueryContent(Map<String, String> queryContent) {
        this.queryContent = queryContent;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    public String getScroll() {
        return scroll;
    }

    public void setScroll(String scroll) {
        this.scroll = scroll;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "AppInfoQueryParms{" +
                "indices=" + indices +
                ", type='" + type + '\'' +
                ", size=" + size +
                ", page=" + page +
                ", total=" + total +
                ", scrollId='" + scrollId + '\'' +
                ", scroll='" + scroll + '\'' +
                ", queryContent=" + queryContent +
                '}';
    }
}
