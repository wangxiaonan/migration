package com.wangxiaonan.migration.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局的进度数据
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/20
 **/
public class GlobalProgressList {
    public static final String PROGRESS_NAME_APP_INFO = "app_info_migration";

    private static Map<String, Progress> progressMap;

    private GlobalProgressList() {
    }

    public static synchronized Map<String, Progress> getInstance() {
        if (progressMap == null) {
            progressMap = new HashMap<>();
        }
        return progressMap;
    }

}
