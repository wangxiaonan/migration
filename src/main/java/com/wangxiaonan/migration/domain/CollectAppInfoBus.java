package com.wangxiaonan.migration.domain;

import java.util.Arrays;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/24
 **/
public class CollectAppInfoBus extends Base {
    private String index;
    private int score;
    private String className;
    private String source;
    private String[] reqParam;
    private String type;
    private String sysName;
    private String methodName;
    private String status;
    private String recordTime;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String[] getReqParam() {
        return reqParam;
    }

    public void setReqParam(String[] reqParam) {
        this.reqParam = reqParam;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    @Override
    public String getBody() {
        StringBuffer sb = new StringBuffer();
        for (String r : reqParam) {
            sb.append(r);
        }
        return sb.toString();
    }

    @Override
    public String getDatetime() {
        return this.getRecordTime();
    }

    @Override
    public String toString() {
        return "CollectAppInfoBus{" +
                "id='" + super.getId() + '\'' +
                ", index='" + index + '\'' +
                ", score=" + score +
                ", className='" + className + '\'' +
                ", source='" + source + '\'' +
                ", reqParam=" + Arrays.toString(reqParam) +
                ", type='" + type + '\'' +
                ", sysName='" + sysName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
