package com.wangxiaonan.migration.domain;

/**
 * elasticSearch 映射对象
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
public class CollectAppInfo extends Base {
    private String index;
    private int score;
    private String offset;
    private String method;
    private String level;
    private String IP;
    private String input_type;
    private String source;
    private String thread;
    private String type;
    private String threadId;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getInput_type() {
        return input_type;
    }

    public void setInput_type(String input_type) {
        this.input_type = input_type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    @Override
    public String toString() {
        return "CollectAppInfo{" +
                "id='" + super.getId() + '\'' +
                ", index='" + index + '\'' +
                ", score=" + score +
                ", offset='" + offset + '\'' +
                ", method='" + method + '\'' +
                ", level='" + level + '\'' +
                ", IP='" + IP + '\'' +
                ", input_type='" + input_type + '\'' +
                ", source='" + source + '\'' +
                ", thread='" + thread + '\'' +
                ", type='" + type + '\'' +
                ", body='" + super.getBody() + '\'' +
                ", threadId='" + threadId + '\'' +
                ", datetime='" + super.getDatetime() + '\'' +
                '}';
    }
}
