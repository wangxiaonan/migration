package com.wangxiaonan.migration.domain;

import org.springframework.data.domain.Page;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
public class QueryResult<T> {
    
    private Page<T> page;
    private String scrollId;
    private String scroll;

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    public String getScroll() {
        return scroll;
    }

    public void setScroll(String scroll) {
        this.scroll = scroll;
    }
}
