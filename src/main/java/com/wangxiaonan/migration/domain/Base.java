package com.wangxiaonan.migration.domain;

import io.searchbox.annotations.JestId;

import java.io.Serializable;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/24
 **/
public class Base implements Serializable {
    @JestId
    private String id;

    private String body;

    private String datetime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
