package com.wangxiaonan.migration.domain;

import org.springframework.data.domain.Pageable;

import java.io.Serializable;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
public class SearchParams implements Serializable {
    private String contents;
    private String indices;
    private Pageable pageable;
    private String scrollId;
    private String scroll;

    public SearchParams() {
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getIndices() {
        return indices;
    }

    public void setIndices(String indices) {
        this.indices = indices;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    public String getScroll() {
        return scroll;
    }

    public void setScroll(String scroll) {
        this.scroll = scroll;
    }

    @Override
    public String toString() {
        return "SearchParams{" +
                "contents='" + contents + '\'' +
                ", indices='" + indices + '\'' +
                ", pageable=" + pageable +
                ", scrollId='" + scrollId + '\'' +
                ", scroll='" + scroll + '\'' +
                '}';
    }
}
