package com.wangxiaonan.migration.domain;

/**
 * vpp数据库映射对象
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 */
public class AppInformation {
    private Integer id;

    private String appNameList;

    private String custName;

    private String deviceUniqueId;

    private String mobile;

    private String idNo;

    private Integer createDate;

    private String createTime;

    private Integer updateDate;

    private String updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppNameList() {
        return appNameList;
    }

    public void setAppNameList(String appNameList) {
        this.appNameList = appNameList == null ? null : appNameList.trim();
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName == null ? null : custName.trim();
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId == null ? null : deviceUniqueId.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    public Integer getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Integer createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Integer updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    @Override
    public String toString() {
        return "AppInformation{" +
                "id=" + id +
                ", appNameList='" + appNameList + '\'' +
                ", custName='" + custName + '\'' +
                ", deviceUniqueId='" + deviceUniqueId + '\'' +
                ", mobile='" + mobile + '\'' +
                ", idNo='" + idNo + '\'' +
                ", createDate=" + createDate +
                ", createTime='" + createTime + '\'' +
                ", updateDate=" + updateDate +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}