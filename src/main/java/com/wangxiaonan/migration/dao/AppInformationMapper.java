package com.wangxiaonan.migration.dao;

import com.wangxiaonan.migration.domain.AppInformation;

public interface AppInformationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AppInformation record);

    int insertSelective(AppInformation record);

    AppInformation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AppInformation record);

    int updateByPrimaryKey(AppInformation record);
}