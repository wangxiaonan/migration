package com.wangxiaonan.migration.config;

import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;

/**
 * freemarker configuration
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
@Component
public class FreemarkerConfig {
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @PostConstruct
    public void setSharedVariable() throws TemplateModelException {
        /*
         * AUTO_DETECT_TAG_SYNTAX: 根据第一个出现的标签判断使用 <> 或者 []
         * ANGLE_BRACKET_TAG_SYNTAX: 使用 <>
         * SQUARE_BRACKET_TAG_SYNTAX: 使用 []
         */
        freeMarkerConfigurer.getConfiguration().setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);
    }
}
