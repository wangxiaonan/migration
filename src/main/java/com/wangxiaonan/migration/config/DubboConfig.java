package com.wangxiaonan.migration.config;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * dubbo configuration
 * http://dubbo.apache.org/#!/docs/user/configuration/annotation.md?lang=zh-cn
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
@Configuration
public class DubboConfig {
    @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("migration-app-info");
        applicationConfig.setLogger("slf4j");
        return applicationConfig;
    }

    @Bean
    public ConsumerConfig consumerConfig() {
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setTimeout(5000);
        return consumerConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
//        registryConfig.setAddress("zookeeper://47.104.17.185:2181");
        registryConfig.setAddress("zookeeper://10.10.8.81:2181");
        registryConfig.setClient("zkclient");
        registryConfig.setProtocol("dubbo");
        return registryConfig;
    }
}
