package com.wangxiaonan.migration.config;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ElasticSearch JestClient Bean
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
//@Configuration
public class JestClientConfig {
    private static final Logger logger = LoggerFactory.getLogger(JestClientConfig.class);

//    @Bean("JestClient")
    public JestClient jestClient() {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder("http://localhost:9200")
                .multiThreaded(true)
                //Per default this implementation will create no more than 2 concurrent connections per given route
                .defaultMaxTotalConnectionPerRoute(2)
                // and no more 20 connections in total
                .maxTotalConnection(2)
                .connTimeout(10000)
                .readTimeout(10000)
                .build());
        logger.info("[JestClientConfig] create JestClient sucessfully ");
        return factory.getObject();
    }
}
