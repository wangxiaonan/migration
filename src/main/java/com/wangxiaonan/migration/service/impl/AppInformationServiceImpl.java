package com.wangxiaonan.migration.service.impl;

import com.wangxiaonan.migration.dao.AppInformationMapper;
import com.wangxiaonan.migration.domain.AppInformation;
import com.wangxiaonan.migration.service.AppInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
@Service
@Transactional
public class AppInformationServiceImpl implements AppInformationService {
    private static final Logger logger = LoggerFactory.getLogger(AppInformationServiceImpl.class);
    @Autowired
    private AppInformationMapper appInformationDao;

    @Override
    public int insert(AppInformation record) {
        int rs = 0;
        try {
            rs = appInformationDao.insert(record);
        } catch (Exception e) {
            logger.error("[AppInformationServiceImpl ==> insert] {}", record);
            throw e;
        }
        return rs;
    }
}
