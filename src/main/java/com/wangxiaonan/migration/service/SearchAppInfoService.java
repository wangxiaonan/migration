package com.wangxiaonan.migration.service;

import com.wangxiaonan.migration.domain.Base;
import com.wangxiaonan.migration.domain.SearchParams;
import org.springframework.data.domain.Page;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
public interface SearchAppInfoService {
    <T extends Base> Page<T> searchAppInfo(Class<T> clazz, SearchParams searchParams);

    /**
     * 使用size, from分页，最多10000条数据，现舍弃
     *
     * @param searchParams
     * @return
     */
    @Deprecated
    String migrationAppInfo(SearchParams searchParams);

    /**
     * 使用scroll分页，适用于查询全部数据，没有条数限制
     *
     * @param searchParams
     * @return
     */
    <T extends Base> String migrationAppInfoByScroll(Class<T> clazz, SearchParams searchParams);
}
