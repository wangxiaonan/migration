package com.wangxiaonan.migration.service;

import com.wangxiaonan.migration.domain.Base;
import com.wangxiaonan.migration.domain.QueryResult;
import com.wangxiaonan.migration.dto.AppInfoQueryParms;
import org.springframework.data.domain.Page;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/17
 **/
public interface ESService {
    /**
     * @param queryContent
     * @return
     */
    <T extends Base> Page<T> searchInfo(Class<T> clazz, AppInfoQueryParms queryContent);

    /**
     * scroll分页查询，条数可以超过1w条限制，<br>
     * 注意游标查询每次返回一个新字段 `_scroll_id`，每次我们做下一次游标查询，我们必须把前一次查询返回的字段 `_scroll_id 传递进去。
     * 当没有更多的结果返回的时候，我们就处理完所有匹配的文档了。
     *
     * @param queryContent
     * @return
     */
    <T extends Base> QueryResult<T> searchInfoByScroll(Class<T> clazz, AppInfoQueryParms queryContent);

}
