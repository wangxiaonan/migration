package com.wangxiaonan.migration.service;

import com.wangxiaonan.migration.domain.AppInformation;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/18
 **/
public interface AppInformationService {
    int insert(AppInformation record);
}
